﻿//Nama  : Niken Ayu Anggarini
//NRP   : 4210181003
//Multi-threading Calculator : SERVER

using System;
using System.IO;
using System.Net;           
using System.Net.Sockets;   //Untuk membuka socket
using System.Text;
using System.Threading;     //Unuk melakukan multi-threading

namespace ServerCalculator
{
    // Class petama untuk proses
    class BaseProses    // Pada class terdapat method dan property, minimal 1 method yaitu public static void main
    {
        public static void Main(String[] args)
        {
            TcpListener server = null;
            bool WaitingConnection = true;
            int counter = 0;    //Menghitung client yang masuk
            try
            {
                // Mengatur TCP Listener pada port 3456
                Int32 port = 3456;

                //TcpListener server = new.TcpListener(port)
                //Di sini, jalur bagi client untuk masuk dibuat
                server = new TcpListener(IPAddress.Any, port);

                //Start lisnteng to client's requests
                //Di sini, jalur tadi dibuka
                server.Start();

                while (WaitingConnection)
                {
                    counter++;
                    Console.WriteLine("Wating for the Clients...");        //Menampilkan pesan

                    //Perform a blocking call to accept clients requests
                    TcpClient client = server.AcceptTcpClient();
                    Console.WriteLine("Client no. {0} has connected.", counter);

                    //Mulai masuk ke dalam Proses
                    CalculatingSession clientThread = new CalculatingSession();
                    clientThread.startClient(client, Convert.ToString(counter));

                }
            }
            // Apabila terjadi kegagalan, maka akan dilempar ke catch
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }

    // Class kedua untuk memproses thread
    class CalculatingSession
    {
        TcpClient clientSocket;
        string clNo;

        public void startClient(TcpClient intClientSocket, string clientNo)
        {
            Thread.Sleep(5);
            this.clientSocket = intClientSocket;
            this.clNo = clientNo;
            Thread ctThread = new Thread(gettingParameter);
            ctThread.Start();
        }

        private void gettingParameter()
        {
            byte[] bytesFrom = new byte[100000025];
            string dataFromClient = null;
            byte[] sendBytes = null;
            string serverResponse = null;
            float finalResult;
            NetworkStream stream = null;
            bool WaitingForStream = true;

            while (WaitingForStream)
            {
                try
                {
                    //Get a stream object for reading and waiting
                    stream = clientSocket.GetStream();
                    stream.Read(bytesFrom, 0, (int)clientSocket.ReceiveBufferSize);
                    dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
                    finalResult = Calculator(dataFromClient);
                    serverResponse = "The Result is " + Convert.ToString(finalResult);
                    Console.WriteLine("The Result is: {0}", finalResult);
                    sendBytes = System.Text.Encoding.ASCII.GetBytes(serverResponse);
                    stream.Write(sendBytes, 0, sendBytes.Length);
                }

                catch (Exception e)
                {
                    stream.Close();
                    clientSocket.Close();
                    WaitingForStream = false;
                    Console.WriteLine(e.ToString());
                }
            }
        }

        private float Calculator(string ArgumentForCalcution)
        {
            string argument = ArgumentForCalcution;
            float numberOne;
            float numberTwo;
            char operand;
            float result = 0;

            //Argument dipisahkan dengan spasi
            string[] splitArgument = argument.Split(' ');

            numberOne = Convert.ToSingle(splitArgument[0]);
            operand = char.Parse(splitArgument[1]);
            numberTwo = Convert.ToSingle(splitArgument[0]);

            switch (operand)
            {
                case '+':
                    result = numberOne + numberTwo;
                    break;
                case '-':
                    result = numberOne - numberTwo;
                    break;
                case '*':
                    result = numberOne * numberTwo;
                    break;
                case '/':
                    result = numberOne / numberTwo;
                    break;
            }
            return result;
        }
    }
}
