﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

class Client
{
    public static void Main(String[] args)
    {
        string message;
        byte[] data;
        //Membuat TcpClient yang bisa dijalankan dengan syarat ada TcpServer yang terkoneki dengan addres yang sama
        Int32 port = 3456;
        TcpClient client = new TcpClient("127.0.0.1", port);

        try
        {
            //Get a client stream for reading and writing
            //Stream stream = client.GetStream()

            NetworkStream stream = client.GetStream();
            Console.WriteLine("Connected to Server");

            while (true)
            {
                Console.WriteLine("Input your digits: ");
                message = Console.ReadLine();
                //Menerjemahkan pesan ke dalam ASCII dan menyimpannya sebagai Byte array
                data = System.Text.Encoding.ASCII.GetBytes(message);

                //Mengirimkan pesan ke TcpServer
                stream.Write(data, 0, data.Length);

                Console.WriteLine("Sent: {0}", message);

                //Menerima respon TcpServer

                //Buffer untuk menyimpan byte data
                data = new Byte[256];

                //String untuk menyimpan respon representasi ASCII
                String responseData = String.Empty;

                //Membaca batch pertama dari byte respon TcpServer
                Int32 bytes = stream.Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                Console.WriteLine("Received: {0}", responseData);
            }

        }
        catch (ArgumentNullException e)
        {
            Console.WriteLine("ArgumentNullException: {0}", e);
        }

        Console.WriteLine("\n Press Enter to continue...");
        Console.Read();
    }
}